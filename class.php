<?php

class Htmalpage{
    protected $title="ex3";
    protected $body="name";
        
    public function view(){
        echo "<html>
        <head>
            <title>$this->title</title>
        </head>
        <body>$this->body</body>
        </html> ";
        }
    function __construct($title="", $body=""){
        if($title != ""){
            $this->title = $title; }
        if($body != ""){
            $this->body = $body; }
    }
}
 
class coloredMessage extends Htmalpage{
    protected $color="black"; // צבע דיפולטיבי
    protected $colors = array('red','pink','yellow','blue','green');

    function __construct($title,$body,$color=""){
        parent::__construct($title, $body);   
        if($color != ""){
            if(in_array($color,$this->colors)){
                $this->color = $color;
            }
            else{
                $this->body="Error! The selected color is invalid";
            } 
         }
        }

    public function view(){
        echo "<p style = 'color: $this->color'>$this->body</p>";
    }
}

class FontSize extends coloredMessage{
    protected $sizeOfFont=10;
    public function __set($property,$value){  //סתר
        if ($property == 'sizeOfFont'){
            $sizes = array(10,11,12,13,14,15,16,17,18,19,20,21,22,23,24);
            if(in_array($value,$sizes)){
                $this->sizeOfFont = $value;
            }
            else{
                $this->body="Error! The selected size is invalid";
            }   
        }
    }
    public function viewOfSize(){ 
        echo "<p style = 'color:$this->color;font-size: $this->sizeOfFont'>$this->body</p>";
    }
}
    

?>
